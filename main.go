package main

import ( "net/http"
		 "time" 
		 "log")

func main() {

	StartServer()
	
	// Essa linha deve ser executada sem alteração
	// da função StartServer	
	log.Println("[INFO] Servidor no ar!")
}

func cebolas(w http.ResponseWriter, r *http.Request) {

	w.Write([]byte(r.Method + " " + r.URL.Path + "?" + r.URL.RawQuery))
}

func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
			Addr       : "172.22.51.147:8083",
			IdleTimeout: duration, 
	}

	http.HandleFunc("/", cebolas)

	log.Print(server.ListenAndServe())
}