//Help source: https://stackoverflow.com/questions/47115482/how-do-you-insert-an-image-into-a-html-golang-file
package main


import ( 
		"time"
		"net/http"
		"fmt"
)

func main() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
			Addr       : "172.22.51.15:8092",
			IdleTimeout: duration,
		//	Handler    : 
		}

	http.HandleFunc("/imagem", imagens)
	http.Handle("/imagens/", http.StripPrefix("/imagens/", http.FileServer(http.Dir("./imagens"))))

	server.ListenAndServe()
}

func imagens(w http.ResponseWriter, r *http.Request) {
	
	fmt.Fprintf(w, "<h1>Cebolas!</h1>")
    fmt.Fprintf(w, "<img src='imagens/palhaco.jpg' alt='imagens' style='width:1280px;height:720px;'>")
}

